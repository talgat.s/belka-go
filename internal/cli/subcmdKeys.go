package main

import (
	"crypto/rand"
	"crypto/rsa"
	"encoding/base64"
	"flag"
	"fmt"

	"gitlab.com/talgat.s/belka-go/pkg/keys"
)

type subcmdKeys struct {
	cmd             *flag.FlagSet
	flagBase64      *bool
	flagOnlyPrivate *bool
}

func newSubcmdKeys() subcmd {
	cmd := flag.NewFlagSet(cmdNameKeys, flag.ExitOnError)

	onlyPrivate := cmd.Bool("private-only", false, "Only create private key")
	b64 := cmd.Bool("base64", false, "Convert to base64, but only prints to stdout")

	return &subcmdKeys{
		cmd:             cmd,
		flagBase64:      b64,
		flagOnlyPrivate: onlyPrivate,
	}
}

func (s *subcmdKeys) getCmdName() string {
	return s.cmd.Name()
}

func (s *subcmdKeys) printDefaults() {
	s.cmd.PrintDefaults()
}

func (s *subcmdKeys) parse(args []string) error {
	if err := s.cmd.Parse(args); err != nil {
		return err
	}

	// Check which subcommand was Parsed using the FlagSet.Parsed() function. Handle each case accordingly.
	// FlagSet.Parse() will evaluate to false if no flags were parsed (i.e. the user did not provide any flags)
	if !s.cmd.Parsed() {
		return fmt.Errorf("please provide correct arguments to %s command", s.cmd.Name())
	}

	// create key
	key, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return err
	}

	// convert to strings
	privatePem := keys.StringifyPrivate(key)
	publicPem, err := keys.StringifyPublic(key.Public())
	if err != nil {
		return err
	}

	if *s.flagBase64 {
		printKeys(
			*s.flagOnlyPrivate,
			base64.StdEncoding.EncodeToString(privatePem),
			base64.StdEncoding.EncodeToString(publicPem),
		)
	} else {
		printKeys(*s.flagOnlyPrivate, string(privatePem), string(publicPem))
	}

	return nil
}

func printKeys(onlyPrivate bool, privateKey, publicKey string) {
	fmt.Printf(
		`
Private key:

%s
				`,
		privateKey,
	)

	if !onlyPrivate {
		fmt.Printf(
			`
Public key:

%s
				`,
			publicKey,
		)
	}
}
