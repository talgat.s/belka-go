package game

func getPlayersSuit(initialPlayer Player) map[Player]Suit {
	playersSuit := make(map[Player]Suit)

	playersSuit[initialPlayer] = club
	playersSuit[initialPlayer.GetNext()] = heart
	playersSuit[initialPlayer.GetNext().GetNext()] = spade
	playersSuit[initialPlayer.GetNext().GetNext().GetNext()] = diamond

	return playersSuit
}

func getRoundResult(
	opt *Options,
	rounds []Round,
	winner Team,
	winnerPoints Points,
	hasTrumpSuit bool,
) roundResult {
	switch {
	case winner == nil:
		return roundDraw
	case rounds != nil && len(rounds) == 0:
		return roundOne
	case rounds != nil && rounds[len(rounds)-1].GetWinner() == nil:
		switch {
		case hasTrumpSuit && winnerPoints.getOpponents() >= opt.ReliablePoints:
			return roundPrevDrawWithTrumpSuitWithOpponentReliablePoints
		case hasTrumpSuit && winnerPoints.getOpponents() < opt.ReliablePoints:
			return roundPrevDrawWithTrumpSuitWithoutOpponentReliablePoints
		case !hasTrumpSuit && winnerPoints.getOpponents() >= opt.ReliablePoints:
			return roundPrevDrawWithoutTrumpSuitWithOpponentReliablePoints
		case !hasTrumpSuit && winnerPoints.getOpponents() < opt.ReliablePoints:
			return roundPrevDrawWithoutTrumpSuitWithoutOpponentReliablePoints
		default:
			return roundDraw
		}
	case hasTrumpSuit && winnerPoints.getOpponents() >= opt.ReliablePoints:
		return roundWithTrumpSuitWithOpponentReliablePoints
	case hasTrumpSuit && winnerPoints.getOpponents() < opt.ReliablePoints:
		return roundWithTrumpSuitWithoutOpponentReliablePoints
	case !hasTrumpSuit && winnerPoints.getOpponents() >= opt.ReliablePoints:
		return roundWithoutTrumpSuitWithOpponentReliablePoints
	case !hasTrumpSuit && winnerPoints.getOpponents() < opt.ReliablePoints:
		return roundWithoutTrumpSuitWithoutOpponentReliablePoints
	default:
		return roundDraw
	}
}

func getWinnerTeam(ps map[Team]Points, ts [NumTeamsPerGame]Team) Team {
	if ps[ts[0]] > ps[ts[1]] {
		return ts[0]
	} else if ps[ts[0]] < ps[ts[1]] {
		return ts[1]
	}

	return nil
}

func getTrumpSuitPlayer(hands map[Player]*Hand) (Player, error) {
	return getPlayerWithJackOfClub(hands)
}
