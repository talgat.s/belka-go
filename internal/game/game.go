package game

import (
	"github.com/google/uuid"
)

type belka struct {
	id          uuid.UUID
	pub         Publisher
	sub         Subscriber
	opt         *Options
	rounds      []Round
	teams       [NumTeamsPerGame]Team
	playersSuit map[Player]Suit
	scores      TeamsScore
	winner      Team
}

func NewGame(pub Publisher, sub Subscriber, opt *Options, ts [NumTeamsPerGame]Team) Game {
	return &belka{
		id:     uuid.New(),
		pub:    pub,
		sub:    sub,
		opt:    opt,
		teams:  ts,
		scores: newTeamsScore(),
	}
}

func (g *belka) GetID() uuid.UUID {
	return g.id
}

func (g *belka) GetPub() Publisher {
	return g.pub
}

func (g *belka) GetSub() Subscriber {
	return g.sub
}

func (g *belka) getOptions() *Options {
	return g.opt
}

func (g *belka) setOptions(opt *Options) *belka {
	g.opt = opt

	return g
}

func (g *belka) GetRounds() []Round {
	return g.rounds
}

func (g *belka) setRounds(rounds []Round) *belka {
	g.rounds = rounds

	return g
}

func (g *belka) GetTeams() [NumTeamsPerGame]Team {
	return g.teams
}

func (g *belka) setTeams(teams [NumTeamsPerGame]Team) *belka {
	g.teams = teams

	return g
}

func (g *belka) getPlayersSuit() map[Player]Suit {
	return g.playersSuit
}

func (g *belka) setPlayersSuit(playersSuit map[Player]Suit) gameInRound {
	g.playersSuit = playersSuit

	if g.GetSub().GetOnPlayersSuitChange() != nil {
		g.GetSub().GetOnPlayersSuitChange() <- g.playersSuit
	}

	return g
}

func (g *belka) getPlayerSuit(player Player) Suit {
	return g.playersSuit[player]
}

func (g *belka) getScores() TeamsScore {
	return g.scores
}

func (g *belka) setScores(scores TeamsScore) *belka {
	g.scores = scores

	if g.GetSub().GetOnScoresChange() != nil {
		g.GetSub().GetOnScoresChange() <- g.scores
	}

	return g
}

func (g *belka) GetWinner() Team {
	return g.winner
}

func (g *belka) setWinner(winner Team) *belka {
	g.winner = winner

	return g
}

func (g *belka) Start() error {
	if g.GetSub().GetOnGameStart() != nil {
		g.GetSub().GetOnGameStart() <- g
	}

	if err := g.runRounds(); err != nil {
		return err
	} else {
		g.setWinner(g.getScores().getWinner())

		if g.GetSub().GetOnGameEnd() != nil {
			g.GetSub().GetOnGameEnd() <- g
		}

		g.GetPub().Close()
		g.GetSub().Close()

		return nil
	}
}

func (g *belka) runRounds() error {
	firstPlayer := g.GetTeams()[0].GetMembers()[0]

	for !g.getScores().isEnd() {
		r := newRound(g.GetPub(), g.GetSub(), g)

		g.setRounds(append(g.GetRounds(), r))

		if err := r.start(firstPlayer); err != nil {
			return err
		} else {
			firstPlayer = firstPlayer.GetNext()
			g.setScores(addScore(g.scores, r.GetWinner(), r.getScore()))
		}
	}

	return nil
}

func addScore(scores TeamsScore, t Team, s score) TeamsScore {
	return scores.setScore(t, scores.GetScore(t)+s)
}
