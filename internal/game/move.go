package game

import (
	"github.com/google/uuid"
)

type moveObject struct {
	id     uuid.UUID
	pub    Publisher
	sub    Subscriber
	lap    lapInMove
	player Player
	card   Card
}

func newMove(pub Publisher, sub Subscriber, l lapInMove, pl Player) Move {
	return &moveObject{
		id:     uuid.New(),
		pub:    pub,
		sub:    sub,
		lap:    l,
		player: pl,
	}
}

func (m *moveObject) GetID() uuid.UUID {
	return m.id
}

func (m *moveObject) getPub() Publisher {
	return m.pub
}

func (m *moveObject) getSub() Subscriber {
	return m.sub
}

func (m *moveObject) getLap() lapInMove {
	return m.lap
}

func (m *moveObject) setLap(lap lapInMove) *moveObject {
	m.lap = lap

	return m
}

func (m *moveObject) GetPlayer() Player {
	return m.player
}

func (m *moveObject) setPlayer(player Player) *moveObject {
	m.player = player

	return m
}

func (m *moveObject) GetCard() Card {
	return m.card
}

func (m *moveObject) setCard(card Card) *moveObject {
	m.card = card

	return m
}

func (m *moveObject) start() error {
	if m.getSub().GetOnMoveStart() != nil {
		m.getSub().GetOnMoveStart() <- m
	}

	if c, err := m.GetPlayer().move(m); err != nil {
		return err
	} else {
		m.setCard(c)
	}

	if m.getSub().GetOnMoveEnd() != nil {
		m.getSub().GetOnMoveEnd() <- m
	}

	return nil
}
