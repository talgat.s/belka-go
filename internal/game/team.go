package game

import "github.com/google/uuid"

const (
	NumPlayersPerTeam = 2
	NumTeamsPerGame   = 2
	NumPlayersPerGame = NumPlayersPerTeam * NumTeamsPerGame
)

type Team interface {
	GetID() uuid.UUID
	GetMembers() [NumPlayersPerTeam]Player
	AssignMembers(members [NumPlayersPerTeam]Player) Team
}

func NewTeams(team1 Team, team2 Team) [NumTeamsPerGame]Team {
	ts := [NumTeamsPerGame]Team{
		team1,
		team2,
	}

	for i, t := range ts {
		for j, p := range t.GetMembers() {
			nextI := (i + 1) % NumTeamsPerGame
			nextJ := (j + ((i + 1) / NumTeamsPerGame)) % len(t.GetMembers())
			ts[nextI].GetMembers()[nextJ].setNext(p)
		}
	}

	return ts
}

type teamObject struct {
	id      uuid.UUID
	members [NumPlayersPerTeam]Player
}

func NewTeam(id uuid.UUID) Team {
	t := &teamObject{
		id: id,
	}

	return t
}

func (t *teamObject) GetID() uuid.UUID {
	return t.id
}

func (t *teamObject) GetMembers() [NumPlayersPerTeam]Player {
	return t.members
}

func (t *teamObject) setMembers(members [NumPlayersPerTeam]Player) *teamObject {
	t.members = members

	return t
}

func (t *teamObject) AssignMembers(members [NumPlayersPerTeam]Player) Team {
	t.setMembers(members)

	for _, m := range t.members {
		if m != nil {
			m.setTeam(t)
		}
	}

	return t
}
