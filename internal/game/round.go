package game

import (
	"fmt"

	"github.com/google/uuid"
)

type roundResult int

const (
	numLapsPerRound = 8

	roundDraw roundResult = iota
	roundOne
	roundPrevDrawWithTrumpSuitWithOpponentReliablePoints
	roundPrevDrawWithTrumpSuitWithoutOpponentReliablePoints
	roundPrevDrawWithoutTrumpSuitWithOpponentReliablePoints
	roundPrevDrawWithoutTrumpSuitWithoutOpponentReliablePoints
	roundWithTrumpSuitWithOpponentReliablePoints
	roundWithTrumpSuitWithoutOpponentReliablePoints
	roundWithoutTrumpSuitWithOpponentReliablePoints
	roundWithoutTrumpSuitWithoutOpponentReliablePoints
)

type roundObject struct {
	id        uuid.UUID
	pub       Publisher
	sub       Subscriber
	game      gameInRound
	laps      [numLapsPerRound]Lap
	trumpSuit Suit
	hands     map[Player]*Hand // NOTE: Player map of Card to whether if it is played before
	points    map[Team]Points
	winner    Team
	score     score
	result    roundResult
}

func newRound(pub Publisher, sub Subscriber, g gameInRound) Round {
	return &roundObject{
		id:     uuid.New(),
		pub:    pub,
		sub:    sub,
		game:   g,
		hands:  newHands(g.GetTeams()),
		points: newPoints(g.GetTeams()),
	}
}

func newHands(ts [NumTeamsPerGame]Team) map[Player]*Hand {
	ph := make(map[Player]*Hand)

	for _, t := range ts {
		for _, p := range t.GetMembers() {
			ph[p] = &Hand{}
		}
	}

	return ph
}

func newPoints(ts [NumTeamsPerGame]Team) map[Team]Points {
	ps := make(map[Team]Points)

	for _, t := range ts {
		ps[t] = 0
	}

	return ps
}

func (r *roundObject) GetID() uuid.UUID {
	return r.id
}

func (r *roundObject) getPub() Publisher {
	return r.pub
}

func (r *roundObject) setPub(pub Publisher) *roundObject {
	r.pub = pub

	return r
}

func (r *roundObject) getSub() Subscriber {
	return r.sub
}

func (r *roundObject) setSub(sub Subscriber) *roundObject {
	r.sub = sub

	return r
}

func (r *roundObject) getGame() gameInRound {
	return r.game
}

func (r *roundObject) setGame(g gameInRound) *roundObject {
	r.game = g

	return r
}

func (r *roundObject) getLaps() [numLapsPerRound]Lap {
	return r.laps
}

func (r *roundObject) setLaps(laps [numLapsPerRound]Lap) *roundObject {
	r.laps = laps

	return r
}

func (r *roundObject) setLap(i int, l Lap) *roundObject {
	r.laps[i] = l

	return r.setLaps(r.laps)
}

func (r *roundObject) GetHands() map[Player]*Hand {
	return r.hands
}

func (r *roundObject) setHands(hands map[Player]*Hand) *roundObject {
	r.hands = hands

	if r.getSub().GetOnHandsChange() != nil {
		r.getSub().GetOnHandsChange() <- r.hands
	}

	return r
}

func (r *roundObject) getHand(pl Player) *Hand {
	return r.GetHands()[pl]
}

func (r *roundObject) setHand(pl Player, h *Hand) *roundObject {
	r.hands[pl] = h

	return r.setHands(r.hands)
}

func (r *roundObject) GetPoints() map[Team]Points {
	return r.points
}

func (r *roundObject) setPoints(ps map[Team]Points) *roundObject {
	r.points = ps

	return r
}

func (r *roundObject) getPoint(t Team) Points {
	return r.GetPoints()[t]
}

func (r *roundObject) setPoint(t Team, p Points) *roundObject {
	r.points[t] = p

	return r.setPoints(r.points)
}

func (r *roundObject) getTrumpSuit() Suit {
	return r.trumpSuit
}

func (r *roundObject) setTrumpSuit(trumpSuitPlayer Player) error {
	if r.getGame().getPlayersSuit() == nil || len(r.getGame().getPlayersSuit()) < NumPlayersPerGame {
		r.getGame().setPlayersSuit(getPlayersSuit(trumpSuitPlayer))
	}

	r.trumpSuit = r.getGame().getPlayerSuit(trumpSuitPlayer)

	if r.getSub().GetOnTrumpSuitChange() != nil {
		r.getSub().GetOnTrumpSuitChange() <- r.trumpSuit
	}

	return nil
}

func (r *roundObject) GetWinner() Team {
	return r.winner
}

func (r *roundObject) setWinner(winner Team) *roundObject {
	r.winner = winner

	return r
}

func (r *roundObject) getScore() score {
	return r.score
}

func (r *roundObject) setScore(s score) *roundObject {
	r.score = s

	return r
}

func (r *roundObject) getResult() roundResult {
	return r.result
}

func (r *roundObject) setResult(result roundResult) *roundObject {
	r.result = result

	return r
}

func (r *roundObject) start(firstPlayer Player) error {
	if r.getSub().GetOnRoundStart() != nil {
		r.getSub().GetOnRoundStart() <- r
	}

	if err := r.deal(r.getGame().GetTeams()); err != nil {
		return err
	} else if trumpSuitPlayer, err := getTrumpSuitPlayer(r.GetHands()); err != nil {
		return err
	} else if err := r.setTrumpSuit(trumpSuitPlayer); err != nil {
		return err
	} else if err := r.runLaps(firstPlayer, trumpSuitPlayer); err != nil {
		return err
	} else {
		r.setWinner(getWinnerTeam(r.GetPoints(), r.getGame().GetTeams()))
		r.setResult(
			getRoundResult(
				r.getGame().getOptions(),
				r.getGame().GetRounds(),
				r.GetWinner(),
				r.getPoint(r.GetWinner()),
				r.GetWinner() == trumpSuitPlayer.GetTeam(),
			),
		)
		r.setScore(getScore(r.getGame().getOptions(), r.getResult()))

		if r.getSub().GetOnRoundEnd() != nil {
			r.getSub().GetOnRoundEnd() <- r
		}

		return nil
	}
}

func (r *roundObject) deal(ts [NumTeamsPerGame]Team) error {
	r.setHands(dealPlayersCards(ts[0].GetMembers()[0]))

	return nil
}

func (r *roundObject) runLaps(firstPlayer Player, trumpSuitPlayer Player) error {
	trumpSuit := r.getGame().getPlayersSuit()[trumpSuitPlayer]
	firstLapPlayer := firstPlayer

	for i := 0; i < numLapsPerRound; i++ {
		l := newLap(r.getPub(), r.getSub(), r)
		r.setLap(i, l)

		if err := l.start(firstLapPlayer, trumpSuit); err != nil {
			return err
		} else {
			t := l.getWinner().GetTeam()
			r.setPoint(t, r.getPoint(t)+l.getPoints())
			firstLapPlayer = l.getWinner()
		}
	}

	return nil
}

func (r *roundObject) removeCardFromHand(pl Player, card Card) error {
	for i, c := range r.getHand(pl) {
		if c != nil && card.isEqual(*c) {
			r.getHand(pl)[i] = nil
			return nil
		}
	}

	return fmt.Errorf("card %s was already removed, please contact developers", card.String())
}
