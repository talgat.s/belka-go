package game

import "github.com/google/uuid"

type Game interface {
	GetID() uuid.UUID
	GetPub() Publisher
	GetSub() Subscriber
	GetRounds() []Round
	GetTeams() [NumTeamsPerGame]Team
	GetWinner() Team
	Start() error
}

type gameInRound interface {
	getOptions() *Options
	GetRounds() []Round
	GetTeams() [NumTeamsPerGame]Team
	getPlayersSuit() map[Player]Suit
	setPlayersSuit(playersSuit map[Player]Suit) gameInRound
	getPlayerSuit(player Player) Suit
}

type Round interface {
	GetID() uuid.UUID
	GetHands() map[Player]*Hand
	GetPoints() map[Team]Points
	GetWinner() Team
	getScore() score
	start(firstPlayer Player) error
}

type roundInLap interface {
	GetHands() map[Player]*Hand
	removeCardFromHand(pl Player, card Card) error
}

type Lap interface {
	GetID() uuid.UUID
	getWinner() Player
	getPoints() Points
	start(firstPlayer Player, trumpSuit Suit) error
}

type lapInMove interface {
	getRound() roundInLap
}

type Move interface {
	GetID() uuid.UUID
	getPub() Publisher
	getLap() lapInMove
	GetPlayer() Player
	GetCard() Card
	start() error
}

type closeable interface {
	Close()
}
