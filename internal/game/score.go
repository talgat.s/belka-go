package game

import (
	"fmt"

	"gitlab.com/talgat.s/belka-go/pkg/utils"
)

type score int

const (
	scoreMax score = 12

	scoreWithTrumpAndOpponentWithReliablePoints       score = 1
	scoreWithTrumpAndOpponentWithoutReliablePoints    score = 2
	scoreWithoutTrumpAndOpponentWithReliablePoints    score = 2
	scoreWithoutTrumpAndOpponentWithoutReliablePoints score = 3
	scoreForFirstRound                                score = 2
)

func (s score) isMax() bool {
	return s >= scoreMax
}

func (s score) String() string {
	return fmt.Sprintf("%d", int(s))
}

type TeamsScore interface {
	GetScore(t Team) score
	setScore(t Team, s score) TeamsScore
	GetTeams() []Team
	getWinner() Team
	isEnd() bool
}

type teamsScoreObject map[Team]score

func newTeamsScore() *teamsScoreObject {
	return &teamsScoreObject{}
}

func (ts *teamsScoreObject) GetScore(t Team) score {
	return utils.ToValue(ts)[t]
}

func (ts *teamsScoreObject) setScore(t Team, s score) TeamsScore {
	utils.ToValue(ts)[t] = s

	return ts
}

func (ts *teamsScoreObject) GetTeams() []Team {
	var teams []Team

	for t := range *ts {
		teams = append(teams, t)
	}

	return teams
}

func (ts *teamsScoreObject) getWinner() Team {
	for t, s := range *ts {
		if s.isMax() {
			return t
		}
	}

	return nil
}

func (ts *teamsScoreObject) isEnd() bool {
	for _, s := range *ts {
		if s.isMax() {
			return true
		}
	}

	return false
}

func getScore(opt *Options, result roundResult) score {
	switch result {
	case roundDraw:
		return score(0)
	case roundOne:
		return scoreForFirstRound
	case roundPrevDrawWithTrumpSuitWithOpponentReliablePoints:
		return handleDrawScore(opt, getScore(opt, roundWithTrumpSuitWithOpponentReliablePoints))
	case roundPrevDrawWithTrumpSuitWithoutOpponentReliablePoints:
		return handleDrawScore(opt, getScore(opt, roundWithTrumpSuitWithoutOpponentReliablePoints))
	case roundPrevDrawWithoutTrumpSuitWithOpponentReliablePoints:
		return handleDrawScore(opt, getScore(opt, roundWithoutTrumpSuitWithOpponentReliablePoints))
	case roundPrevDrawWithoutTrumpSuitWithoutOpponentReliablePoints:
		return handleDrawScore(opt, getScore(opt, roundWithoutTrumpSuitWithoutOpponentReliablePoints))
	case roundWithTrumpSuitWithOpponentReliablePoints:
		return scoreWithTrumpAndOpponentWithReliablePoints
	case roundWithTrumpSuitWithoutOpponentReliablePoints:
		return scoreWithTrumpAndOpponentWithoutReliablePoints
	case roundWithoutTrumpSuitWithOpponentReliablePoints:
		return scoreWithoutTrumpAndOpponentWithReliablePoints
	case roundWithoutTrumpSuitWithoutOpponentReliablePoints:
		return scoreWithoutTrumpAndOpponentWithoutReliablePoints
	default:
		return score(0)
	}
}

func handleDrawScore(opt *Options, scr score) score {
	switch opt.AfterDraw {
	case DrawMultiplyByTwo:
		return scr * 2
	case DrawAddTwo:
		return scr + 2
	default:
		return scr
	}
}
