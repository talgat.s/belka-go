package game

import (
	"fmt"
	"math/rand"
	"strings"
	"time"

	"gitlab.com/talgat.s/belka-go/pkg/compare"
	"gitlab.com/talgat.s/belka-go/pkg/utils"
)

type Suit int

type Rank int

const (
	diamond Suit = iota
	heart
	spade
	club
)

const (
	seven Rank = iota
	eight
	nine
	jack
	queen
	king
	ten
	ace
)

var suits = [4]Suit{club, spade, heart, diamond}

var ranks = [8]Rank{seven, eight, nine, ten, jack, queen, king, ace}

type Card struct {
	suit Suit
	rank Rank
}

const HandLen = len(suits) * len(ranks) / NumPlayersPerGame

type Hand = [HandLen]*Card

type deck [len(suits) * len(ranks)]Card

var sortedDeck = deck{}

func init() {
	for i, suit := range suits {
		for j, rank := range ranks {
			sortedDeck[(len(ranks)*i)+j] = Card{suit, rank}
		}
	}
}

func newShuffledDeck() deck {
	shuffledDeck := sortedDeck

	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	r.Shuffle(
		len(shuffledDeck),
		func(i, j int) { shuffledDeck[i], shuffledDeck[j] = shuffledDeck[j], shuffledDeck[i] },
	)

	return shuffledDeck
}

func (s Suit) string() string {
	switch s {
	case diamond:
		return "♦︎"
	case heart:
		return "♥︎"
	case spade:
		return "♠︎"
	case club:
		return "♣︎"
	default:
		return ""
	}
}

func toSuit(s string) Suit {
	switch {
	case strings.Contains(s, "♦︎"):
		return diamond
	case strings.Contains(s, "♥︎"):
		return heart
	case strings.Contains(s, "♠︎"):
		return spade
	case strings.Contains(s, "♣︎"):
		return club
	default:
		return 0
	}
}

func (r Rank) string() string {
	switch r {
	case seven:
		return "7"
	case eight:
		return "8"
	case nine:
		return "9"
	case ten:
		return "10"
	case jack:
		return "J"
	case queen:
		return "Q"
	case king:
		return "K"
	case ace:
		return "A"
	default:
		return ""
	}
}

func toRank(s string) Rank {
	switch {
	case strings.Contains(s, "7"):
		return seven
	case strings.Contains(s, "8"):
		return eight
	case strings.Contains(s, "9"):
		return nine
	case strings.Contains(s, "10"):
		return ten
	case strings.Contains(s, "J"):
		return jack
	case strings.Contains(s, "Q"):
		return queen
	case strings.Contains(s, "K"):
		return king
	case strings.Contains(s, "A"):
		return ace
	default:
		return 0
	}
}

func NewCard(suit Suit, rank Rank) Card {
	return Card{
		suit: suit,
		rank: rank,
	}
}

func (c Card) GetSuit() Suit {
	return c.suit
}

func (c Card) GetRank() Rank {
	return c.rank
}

func (c Card) points() Points {
	switch c.rank {
	case ace:
		return Points(11)
	case ten:
		return Points(10)
	case king:
		return Points(4)
	case queen:
		return Points(3)
	case jack:
		return Points(2)
	default:
		return Points(0)
	}
}

func compareJacks(c1, c2 Card, _ Suit) *bool {
	if c1.rank == jack && c2.rank == jack {
		return utils.ToPtr(c1.suit > c2.suit)
	} else if c1.rank == jack {
		return utils.ToPtr(true)
	} else if c2.rank == jack {
		return utils.ToPtr(false)
	}

	return nil
}

func compareTrumps(c1, c2 Card, trumpSuit Suit) *bool {
	if c1.suit == trumpSuit && c2.suit == trumpSuit {
		return utils.ToPtr(c1.rank > c2.rank)
	} else if c1.suit == trumpSuit {
		return utils.ToPtr(true)
	} else if c2.suit == trumpSuit {
		return utils.ToPtr(false)
	}

	return nil
}

func compareRanks(c1, c2 Card, _ Suit) *bool {
	if c1.suit == c2.suit {
		return utils.ToPtr(c1.rank > c2.rank)
	} else {
		return utils.ToPtr(true)
	}
}

func (c Card) isLarger(nextCard Card, trumpSuit Suit) bool {
	fns := []compare.Func[Card, Suit]{
		compareJacks,
		compareTrumps,
		compareRanks,
	}

	return compare.IsLarger(c, nextCard, trumpSuit, fns)
}

func (c Card) isEqual(nextCard Card) bool {
	return c.suit == nextCard.suit && c.rank == nextCard.rank
}

func (c Card) String() string {
	return fmt.Sprintf("%s%s", c.suit.string(), c.rank.string())
}

func ToCard(s string) Card {
	return Card{
		suit: toSuit(s),
		rank: toRank(s),
	}
}

func dealPlayersCards(firstPlayer Player) map[Player]*Hand {
	d := newShuffledDeck()
	hands := make(map[Player]*Hand)
	pl := firstPlayer

	for i, c := range d {
		if hands[pl] == nil {
			hands[pl] = &Hand{}
		}

		card := c
		hands[pl][i/NumPlayersPerGame] = &card
		pl = pl.GetNext()
	}

	return hands
}

func getPlayerWithJackOfClub(hands map[Player]*Hand) (Player, error) {
	for p, h := range hands {
		for _, c := range h {
			if c.suit == club && c.rank == jack {
				return p, nil
			}
		}
	}

	return nil, fmt.Errorf("no jack of club in hands, please contact developers")
}
