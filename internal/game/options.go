package game

type Options struct {
	ReliablePoints Points
	AfterDraw      drawRule
}

type drawRule int

const (
	DrawMultiplyByTwo drawRule = iota
	DrawAddTwo
)
