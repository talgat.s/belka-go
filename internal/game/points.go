package game

type Points int

const (
	maxPoints Points = 120

	ReliablePoints30 Points = 30
	ReliablePoints31 Points = 31
)

func (p Points) getOpponents() Points {
	return maxPoints - p
}
