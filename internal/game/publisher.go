package game

type Publisher interface {
	GetMove() chan Card
	SetMove(move chan Card)

	closeable
}

type publisherObject struct {
	move chan Card
}

func NewPublisher() Publisher {
	return &publisherObject{}
}

func (p *publisherObject) GetMove() chan Card {
	return p.move
}

func (p *publisherObject) SetMove(move chan Card) {
	p.move = move
}

func (p *publisherObject) Close() {
	close(p.GetMove())
}
