package game

import "github.com/google/uuid"

type lapObject struct {
	id     uuid.UUID
	pub    Publisher
	sub    Subscriber
	round  roundInLap
	moves  [NumPlayersPerGame]Move
	winner Player
	points Points
}

func newLap(pub Publisher, sub Subscriber, r roundInLap) Lap {
	return &lapObject{
		id:    uuid.New(),
		pub:   pub,
		sub:   sub,
		round: r,
	}
}

func (l *lapObject) GetID() uuid.UUID {
	return l.id
}

func (l *lapObject) getPub() Publisher {
	return l.pub
}

func (l *lapObject) getSub() Subscriber {
	return l.sub
}

func (l *lapObject) getRound() roundInLap {
	return l.round
}

func (l *lapObject) setRound(round roundInLap) *lapObject {
	l.round = round

	return l
}

func (l *lapObject) getMoves() [NumPlayersPerGame]Move {
	return l.moves
}

func (l *lapObject) setMoves(moves [NumPlayersPerGame]Move) *lapObject {
	l.moves = moves

	return l
}

func (l *lapObject) setMove(i int, m Move) *lapObject {
	l.moves[i] = m
	l.setMoves(l.moves)

	return l
}

func (l *lapObject) getWinner() Player {
	return l.winner
}

func (l *lapObject) setWinner(winner Player) *lapObject {
	l.winner = winner

	return l
}

func (l *lapObject) getPoints() Points {
	return l.points
}

func (l *lapObject) setPoints(points Points) *lapObject {
	l.points = points

	return l
}

func (l *lapObject) start(firstPlayer Player, trumpSuit Suit) error {
	if l.getSub().GetOnLapStart() != nil {
		l.getSub().GetOnLapStart() <- l
	}

	if err := l.runMoves(firstPlayer); err != nil {
		return err
	} else {
		l.setWinner(getWinnerMove(l.getMoves(), trumpSuit).GetPlayer())

		if l.getSub().GetOnLapEnd() != nil {
			l.getSub().GetOnLapEnd() <- l
		}

		return nil
	}
}

func (l *lapObject) runMoves(firstPlayer Player) error {
	currentPlayer := firstPlayer

	for i := 0; i < NumPlayersPerGame; i++ {
		m := newMove(l.getPub(), l.getSub(), l, currentPlayer)
		l.setMove(i, m)

		if err := m.start(); err != nil {
			return err
		} else if err := l.getRound().removeCardFromHand(m.GetPlayer(), m.GetCard()); err != nil {
			return err
		} else {
			l.setPoints(l.getPoints() + m.GetCard().points())
			currentPlayer = currentPlayer.GetNext()
		}
	}

	return nil
}

func getWinnerMove(moves [NumPlayersPerGame]Move, trumpSuit Suit) Move {
	winnerMove := moves[0]

	for _, m := range moves {
		if m.GetCard().isLarger(winnerMove.GetCard(), trumpSuit) {
			winnerMove = m
		}
	}

	return winnerMove
}
