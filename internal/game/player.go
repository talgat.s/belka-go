package game

import "github.com/google/uuid"

type Player interface {
	GetID() uuid.UUID
	GetName() string
	GetTeam() Team
	setTeam(team Team) Player
	GetNext() Player
	setNext(next Player) Player
	move(m Move) (Card, error)
}
