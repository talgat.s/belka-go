package game

import (
	"fmt"

	"github.com/google/uuid"
)

type User struct {
	id   uuid.UUID
	name string
	team Team
	next Player
}

func NewUser(id uuid.UUID, name string) Player {
	return &User{
		id:   id,
		name: name,
	}
}

func (u *User) GetID() uuid.UUID {
	return u.id
}

func (u *User) GetName() string {
	return u.name
}

func (u *User) GetTeam() Team {
	return u.team
}

func (u *User) setTeam(team Team) Player {
	u.team = team

	return u
}

func (u *User) GetNext() Player {
	return u.next
}

func (u *User) setNext(next Player) Player {
	u.next = next

	return u
}

func (u *User) move(m Move) (Card, error) {
	if m.getPub().GetMove() != nil {
		return <-m.getPub().GetMove(), nil
	} else {
		return Card{}, fmt.Errorf("no move was initiated, please contact developers")
	}
}
