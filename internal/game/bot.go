package game

import (
	"fmt"

	"github.com/google/uuid"
)

type Bot struct {
	id   uuid.UUID
	name string
	team Team
	next Player
}

func NewBot(id uuid.UUID, name string) Player {
	return &Bot{
		id:   id,
		name: name,
	}
}

func (b *Bot) GetID() uuid.UUID {
	return b.id
}

func (b *Bot) GetName() string {
	return b.name
}

func (b *Bot) GetTeam() Team {
	return b.team
}

func (b *Bot) setTeam(team Team) Player {
	b.team = team

	return b
}

func (b *Bot) GetNext() Player {
	return b.next
}

func (b *Bot) setNext(next Player) Player {
	b.next = next

	return b
}

func (b *Bot) move(m Move) (Card, error) {
	h := m.getLap().getRound().GetHands()[b]

	for _, c := range h {
		if c != nil {
			return *c, nil
		}
	}

	return Card{}, fmt.Errorf("empty hand, please contact developers")
}
