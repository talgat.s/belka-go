package game

type Subscriber interface {
	GetOnGameStart() chan Game
	SetOnGameStart(onGameStart chan Game)
	GetOnPlayersSuitChange() chan map[Player]Suit
	SetOnPlayersSuitChange(playersSuit chan map[Player]Suit)
	GetOnScoresChange() chan TeamsScore
	SetOnScoresChange(chan TeamsScore)
	GetOnGameEnd() chan Game
	SetOnGameEnd(chan Game)
	GetOnRoundStart() chan Round
	SetOnRoundStart(chan Round)
	GetOnTrumpSuitChange() chan Suit
	SetOnTrumpSuitChange(chan Suit)
	GetOnHandsChange() chan map[Player]*Hand
	SetOnHandsChange(chan map[Player]*Hand)
	GetOnRoundEnd() chan Round
	SetOnRoundEnd(chan Round)
	GetOnLapStart() chan Lap
	SetOnLapStart(chan Lap)
	GetOnLapEnd() chan Lap
	SetOnLapEnd(chan Lap)
	GetOnMoveStart() chan Move
	SetOnMoveStart(chan Move)
	GetOnMoveEnd() chan Move
	SetOnMoveEnd(chan Move)

	closeable
}

type subscriberObject struct {
	onGameStart         chan Game
	onPlayersSuitChange chan map[Player]Suit
	onScoresChange      chan TeamsScore
	onGameEnd           chan Game
	onRoundStart        chan Round
	onTrumpSuitChange   chan Suit
	onHandsChange       chan map[Player]*Hand
	onRoundEnd          chan Round
	onLapStart          chan Lap
	onLapEnd            chan Lap
	onMoveStart         chan Move
	onMoveEnd           chan Move
}

func NewSubscriber() Subscriber {
	return &subscriberObject{}
}

func (s *subscriberObject) GetOnGameStart() chan Game {
	return s.onGameStart
}

func (s *subscriberObject) SetOnGameStart(onGameStart chan Game) {
	s.onGameStart = onGameStart
}

func (s *subscriberObject) GetOnPlayersSuitChange() chan map[Player]Suit {
	return s.onPlayersSuitChange
}

func (s *subscriberObject) SetOnPlayersSuitChange(onPlayersSuitChange chan map[Player]Suit) {
	s.onPlayersSuitChange = onPlayersSuitChange
}

func (s *subscriberObject) GetOnScoresChange() chan TeamsScore {
	return s.onScoresChange
}

func (s *subscriberObject) SetOnScoresChange(onScoresChange chan TeamsScore) {
	s.onScoresChange = onScoresChange
}

func (s *subscriberObject) GetOnGameEnd() chan Game {
	return s.onGameEnd
}

func (s *subscriberObject) SetOnGameEnd(onGameEnd chan Game) {
	s.onGameEnd = onGameEnd
}

func (s *subscriberObject) GetOnRoundStart() chan Round {
	return s.onRoundStart
}

func (s *subscriberObject) SetOnRoundStart(onRoundStart chan Round) {
	s.onRoundStart = onRoundStart
}

func (s *subscriberObject) GetOnTrumpSuitChange() chan Suit {
	return s.onTrumpSuitChange
}
func (s *subscriberObject) SetOnTrumpSuitChange(onTrumpSuitChange chan Suit) {
	s.onTrumpSuitChange = onTrumpSuitChange
}

func (s *subscriberObject) GetOnHandsChange() chan map[Player]*Hand {
	return s.onHandsChange
}

func (s *subscriberObject) SetOnHandsChange(onHandsChange chan map[Player]*Hand) {
	s.onHandsChange = onHandsChange
}

func (s *subscriberObject) GetOnRoundEnd() chan Round {
	return s.onRoundEnd
}

func (s *subscriberObject) SetOnRoundEnd(onRoundEnd chan Round) {
	s.onRoundEnd = onRoundEnd
}

func (s *subscriberObject) GetOnLapStart() chan Lap {
	return s.onLapStart
}

func (s *subscriberObject) SetOnLapStart(onLapStart chan Lap) {
	s.onLapStart = onLapStart
}

func (s *subscriberObject) GetOnLapEnd() chan Lap {
	return s.onLapEnd
}

func (s *subscriberObject) SetOnLapEnd(onLapEnd chan Lap) {
	s.onLapEnd = onLapEnd
}

func (s *subscriberObject) GetOnMoveStart() chan Move {
	return s.onMoveStart
}

func (s *subscriberObject) SetOnMoveStart(onMoveStart chan Move) {
	s.onMoveStart = onMoveStart
}

func (s *subscriberObject) GetOnMoveEnd() chan Move {
	return s.onMoveEnd
}

func (s *subscriberObject) SetOnMoveEnd(onMoveEnd chan Move) {
	s.onMoveEnd = onMoveEnd
}

func (s *subscriberObject) Close() {
	close(s.GetOnGameStart())
	close(s.GetOnPlayersSuitChange())
	close(s.GetOnScoresChange())
	close(s.GetOnGameEnd())
	close(s.GetOnRoundStart())
	close(s.GetOnTrumpSuitChange())
	close(s.GetOnHandsChange())
	close(s.GetOnRoundEnd())
	close(s.GetOnLapStart())
	close(s.GetOnLapEnd())
	close(s.GetOnMoveStart())
	close(s.GetOnMoveEnd())
}
