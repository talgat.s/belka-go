package httputils

import (
	"encoding/json"
	"net/http"
)

// ErrorJSON replies to the request with the specified error message in JSON format and HTTP code.
// It does not otherwise end the request; the caller should ensure no further
// writes are done to w.
func ErrorJSON(w http.ResponseWriter, error interface{}, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(code)
	_ = json.NewEncoder(w).Encode(error)
}
