package auth

import (
	"crypto/rsa"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
)

type accessClaims struct {
	UserID string `json:"user_id,omitempty"`
	Role   string `json:"role,omitempty"`
	jwt.RegisteredClaims
}

func generateAccess(key *rsa.PrivateKey, ttl time.Duration, user *User) (string, error) {
	now := time.Now().UTC()

	c := &accessClaims{
		UserID: user.ID,
		Role:   user.Role,
		RegisteredClaims: jwt.RegisteredClaims{
			ID:        uuid.New().String(),
			Subject:   user.Email,
			ExpiresAt: &jwt.NumericDate{Time: now.Add(ttl)},
			IssuedAt:  &jwt.NumericDate{Time: now},
			NotBefore: &jwt.NumericDate{Time: now},
		},
	}

	token, err := jwt.NewWithClaims(jwt.SigningMethodRS256, c).SignedString(key)
	if err != nil {
		return "", fmt.Errorf("failed to create and sign token: %w", err)
	}

	return token, err
}

func parseAccess(key *rsa.PrivateKey, tokenString string) (*User, error) {
	token, err := jwt.ParseWithClaims(tokenString, &accessClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return key.Public(), nil
	})

	if err != nil {
		return nil, fmt.Errorf("token parsing error: %w", err)
	}

	claims, ok := token.Claims.(*accessClaims)

	if !ok || !token.Valid {
		return nil, fmt.Errorf("invalid jwt")
	}

	return &User{
		ID:    claims.UserID,
		Role:  claims.Role,
		Email: claims.Subject,
	}, nil
}
