package auth

import (
	"crypto/rsa"
	"encoding/base64"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

type User struct {
	ID    string `json:"id,omitempty"`
	Email string `json:"email,omitempty"`
	Role  string `json:"role,omitempty"`
}

type TokenPair struct {
	AccessToken  string
	RefreshToken string
}

func GenerateTokenPair(encodedKey string, accessTokenExp time.Duration, user *User) (*TokenPair, error) {
	key, err := parseKey(encodedKey)
	if err != nil {
		return nil, fmt.Errorf("failed to parse encoded key: %w", err)
	}

	accessToken, err := generateAccess(key, accessTokenExp, user)
	if err != nil {
		return nil, fmt.Errorf("failed to generate access token: %w", err)
	}

	refreshToken, err := generateRefresh(key, user)
	if err != nil {
		return nil, fmt.Errorf("failed to generate refresh token: %w", err)
	}

	return &TokenPair{
		accessToken,
		refreshToken,
	}, nil
}

func ParseRefreshToken(encodedKey string, tokenString string) (*User, error) {
	key, err := parseKey(encodedKey)
	if err != nil {
		return nil, fmt.Errorf("failed to parse encoded key: %w", err)
	}

	user, err := parseRefresh(key, tokenString)
	if err != nil {
		return nil, fmt.Errorf("failed to parse refresh token: %w", err)
	}

	return &User{
		ID: user.ID,
	}, nil
}

func ParseAccessToken(encodedKey string, tokenString string) (*User, error) {
	key, err := parseKey(encodedKey)
	if err != nil {
		return nil, err
	}

	user, err := parseAccess(key, tokenString)
	if err != nil {
		return nil, err
	}

	return &User{
		ID: user.ID,
	}, nil
}

func parseKey(encodedKey string) (*rsa.PrivateKey, error) {
	decodedPrivateKey, err := base64.StdEncoding.DecodeString(encodedKey)
	if err != nil {
		return nil, fmt.Errorf("could not decode key: %w", err)
	}

	key, err := jwt.ParseRSAPrivateKeyFromPEM(decodedPrivateKey)
	if err != nil {
		return nil, fmt.Errorf("create: parse key: %w", err)
	}

	return key, nil
}
