package auth

import (
	"crypto/rsa"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
)

type refreshClaims struct {
	UserID string `json:"user_id,omitempty"`
	jwt.RegisteredClaims
}

func generateRefresh(key *rsa.PrivateKey, user *User) (string, error) {
	now := time.Now().UTC()

	c := &refreshClaims{
		UserID: user.ID,
		RegisteredClaims: jwt.RegisteredClaims{
			ID:        uuid.New().String(),
			Subject:   user.Email,
			IssuedAt:  &jwt.NumericDate{Time: now},
			NotBefore: &jwt.NumericDate{Time: now},
		},
	}

	token, err := jwt.NewWithClaims(jwt.SigningMethodRS256, c).SignedString(key)
	if err != nil {
		return "", fmt.Errorf("failed to create and sign token: %w", err)
	}

	return token, err
}

func parseRefresh(key *rsa.PrivateKey, tokenString string) (*User, error) {
	token, err := jwt.ParseWithClaims(tokenString, &refreshClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return key.Public(), nil
	})

	if err != nil {
		return nil, fmt.Errorf("token parsing error: %w", err)
	}

	claims, ok := token.Claims.(*refreshClaims)

	if !ok || !token.Valid {
		return nil, fmt.Errorf("invalid jwt")
	}

	return &User{
		ID:    claims.UserID,
		Email: claims.Subject,
	}, nil
}
