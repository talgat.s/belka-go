package configs

import (
	"context"
	"flag"

	"github.com/sethvargo/go-envconfig"
)

type RdsConfig struct {
	*SharedConfig `env:",prefix=REDIS_"`
	MaxIdle       int `env:"REDIS_MAX_IDLE,default=80"`
	MaxActive     int `env:"REDIS_MAX_ACTIVE,default=12000"`
}

func newRdsConfig(ctx context.Context) (*RdsConfig, error) {
	c := &RdsConfig{}

	if err := envconfig.Process(ctx, c); err != nil {
		return nil, err
	}

	flag.IntVar(&c.Port, "redis-port", c.Port, "redis port [REDIS_PORT]")
	flag.StringVar(&c.Host, "redis-host", c.Host, "redis port [REDIS_HOST]")
	flag.IntVar(&c.MaxIdle, "redis-max-idle", c.MaxIdle, "redis port [REDIS_MAX_IDLE]")
	flag.IntVar(&c.MaxActive, "redis-max-active", c.MaxActive, "redis port [REDIS_MAX_ACTIVE]")

	return c, nil
}
