package configs

import (
	"context"
	"flag"

	"gitlab.com/talgat.s/belka-go/internal/constant"
)

type SharedConfig struct {
	Port int    `env:"PORT"`
	Host string `env:"HOST,default=localhost"`
}

type Config struct {
	Env constant.Environment
	Api *ApiConfig
	DB  *DBConfig
	Rds *RdsConfig
}

func NewConfig(ctx context.Context) (*Config, error) {
	conf := &Config{
		Env: getEnv(),
	}

	_ = conf.loadDotEnvFiles()

	// Api config
	if c, err := newApiConfig(ctx, conf.Env); err != nil {
		return nil, err
	} else {
		conf.Api = c
	}

	// DB config
	if c, err := newDBConfig(ctx); err != nil {
		return nil, err
	} else {
		conf.DB = c
	}

	// Rds config
	if c, err := newRdsConfig(ctx); err != nil {
		return nil, err
	} else {
		conf.Rds = c
	}

	flag.Parse()

	return conf, nil
}
