package configs

import (
	"context"
	"flag"
	"time"

	"github.com/sethvargo/go-envconfig"

	"gitlab.com/talgat.s/belka-go/internal/constant"
)

type ApiConfig struct {
	*SharedConfig
	Env            constant.Environment
	TokenKey       string        `env:"TOKEN_PRIVATE_KEY"`
	AccessTokenExp time.Duration `env:"ACCESS_TOKEN_EXPIRATION"`
}

func newApiConfig(ctx context.Context, env constant.Environment) (*ApiConfig, error) {
	c := &ApiConfig{
		Env: env,
	}

	if err := envconfig.Process(ctx, c); err != nil {
		return nil, err
	}

	flag.IntVar(&c.Port, "port", c.Port, "server port [PORT]")
	flag.StringVar(&c.TokenKey, "token-key", c.TokenKey, "encoded base64 RSA key [TOKEN_PRIVATE_KEY]")
	flag.DurationVar(
		&c.AccessTokenExp,
		"access-exp",
		c.AccessTokenExp,
		"expiration period for access token, use \"10m\", \"3s\" etc [ACCESS_TOKEN_EXPIRATION]",
	)

	return c, nil
}
