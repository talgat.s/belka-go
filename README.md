# Belka

belka-go is the api for belka.

## Database Migrations

All migrations inside `cmd/db/migrations/`

### Requirements

Install [migrate cli](https://github.com/golang-migrate/migrate/tree/master) tool

```shell
go install -tags 'postgres' github.com/golang-migrate/migrate/v4/cmd/migrate@latest
```

### Adding a migration

```shell
migrate create -ext sql -dir ./cmd/db/migrations/ <MIGRATION_NAME>
```

For example

```shell
migrate create -ext sql -dir ./cmd/db/migrations/ create_users_table
```

It will create 2 files similar to:

```
1481574547_create_users_table.up.sql
1481574547_create_users_table.down.sql
```

Which needs content for migration up and down.

### Migration up

```shell
migrate -path ./cmd/db/migrations/ -database <POSTGRES_URI> up
```

For example

```shell
migrate -path ./cmd/db/migrations/ -database "postgres://belkasuperuser:belka4ever@kz@localhost:5432/app?sslmode=disable" up
```

### Migration down

```shell
migrate -path ./cmd/db/migrations/ -database <POSTGRES_URI> down
```

For example

```shell
migrate -path ./cmd/db/migrations/ -database "postgres://belkasuperuser:belka4ever@kz@localhost:5432/app?sslmode=disable" down
```

### Migration drop

```shell
migrate -path ./cmd/db/migrations/ -database <POSTGRES_URI> drop
```

For example

```shell
migrate -path ./cmd/db/migrations/ -database "postgres://belkasuperuser:belka4ever@kz@localhost:5432/app?sslmode=disable" drop
```

## License

MIT
