package rds

import (
	"fmt"
	"log/slog"
	"time"

	"github.com/gomodule/redigo/redis"

	"gitlab.com/talgat.s/belka-go/cmd/rds/types"
	"gitlab.com/talgat.s/belka-go/configs"
)

type stash struct {
	log  *slog.Logger
	conf *configs.RdsConfig
	pool *redis.Pool
}

func New(log *slog.Logger, conf *configs.RdsConfig) types.Rds {
	s := &stash{
		log:  log,
		conf: conf,
		pool: newPool(conf),
	}

	return s
}

func newPool(conf *configs.RdsConfig) *redis.Pool {
	return &redis.Pool{
		Dial: func() (redis.Conn, error) {
			if c, err := redis.Dial("tcp", fmt.Sprintf("%s:%d", conf.Host, conf.Port)); err != nil {
				return nil, err
			} else {
				return c, nil
			}
		},

		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
		MaxActive: conf.MaxActive,
		MaxIdle:   conf.MaxIdle,
	}
}
func (s *stash) GetPool() *redis.Pool {
	return s.pool
}
