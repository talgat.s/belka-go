package types

import "github.com/gomodule/redigo/redis"

type Rds interface {
	GetPool() *redis.Pool
}
