package handler

import (
	"fmt"
	"net/http"
	"time"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/handler/extension"
	"github.com/99designs/gqlgen/graphql/handler/lru"
	"github.com/99designs/gqlgen/graphql/handler/transport"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/gorilla/websocket"
	"github.com/rs/cors"

	"gitlab.com/talgat.s/belka-go/graph/directive"
	"gitlab.com/talgat.s/belka-go/graph/generated"
	"gitlab.com/talgat.s/belka-go/graph/resolver"
	"gitlab.com/talgat.s/belka-go/internal/game"
)

// GraphQL handler for graphql
func (h *handlerObject) GraphQL() http.Handler {
	c := cors.New(cors.Options{
		AllowedOrigins: []string{
			fmt.Sprintf("http://localhost:%d/", h.api.GetConf().Port),
		},
		AllowCredentials: true,
	})

	srv := handler.New(
		generated.NewExecutableSchema(
			generated.Config{
				Resolvers: &resolver.Resolver{
					Log:   h.api.GetLog(),
					Conf:  h.api.GetConf(),
					DB:    h.db,
					Rds:   h.rds,
					Games: make(map[string]game.Game),
				},
				Directives: generated.DirectiveRoot{
					AllowAccess: directive.AllowAccess,
				},
			},
		),
	)

	srv.AddTransport(transport.Websocket{
		KeepAlivePingInterval: 10 * time.Second,
		Upgrader: websocket.Upgrader{
			CheckOrigin: func(r *http.Request) bool {
				return true
			},
			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
		},
	})
	srv.AddTransport(transport.Options{})
	srv.AddTransport(transport.GET{})
	srv.AddTransport(transport.POST{})
	// srv.AddTransport(transport.MultipartForm{})

	srv.SetQueryCache(lru.New(1000))

	srv.Use(extension.Introspection{})
	srv.Use(extension.AutomaticPersistedQuery{
		Cache: lru.New(100),
	})

	return c.Handler(srv)
}

// Playground handler for graphql playground
func (h *handlerObject) Playground(endpoint string) http.Handler {
	return playground.Handler("GraphQL playground", endpoint)
}
