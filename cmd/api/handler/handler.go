package handler

import (
	"net/http"

	"gitlab.com/talgat.s/belka-go/cmd/api/types"
	dbT "gitlab.com/talgat.s/belka-go/cmd/db/types"
	rdsT "gitlab.com/talgat.s/belka-go/cmd/rds/types"
)

type Handler interface {
	GraphQL() http.Handler
	Playground(endpoint string) http.Handler
}

type handlerObject struct {
	api types.Api
	db  dbT.DB
	rds rdsT.Rds
}

func New(api types.Api, db dbT.DB, rds rdsT.Rds) Handler {
	return &handlerObject{
		api: api,
		db:  db,
		rds: rds,
	}
}
