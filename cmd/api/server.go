package api

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"net"
	"net/http"
	"os"
	"os/signal"

	"gitlab.com/talgat.s/belka-go/cmd/api/router"
	apiT "gitlab.com/talgat.s/belka-go/cmd/api/types"
	dbT "gitlab.com/talgat.s/belka-go/cmd/db/types"
	rdsT "gitlab.com/talgat.s/belka-go/cmd/rds/types"
	"gitlab.com/talgat.s/belka-go/configs"
	"gitlab.com/talgat.s/belka-go/internal/constant"
)

type server struct {
	log  *slog.Logger
	conf *configs.ApiConfig
}

func New(log *slog.Logger, conf *configs.ApiConfig) apiT.Api {
	s := &server{
		log:  log,
		conf: conf,
	}

	return s
}

func (s *server) GetConf() *configs.ApiConfig {
	return s.conf
}

func (s *server) GetLog() *slog.Logger {
	return s.log
}

func (s *server) Start(ctx context.Context, cancel context.CancelFunc, db dbT.DB, rds rdsT.Rds) {
	mux := http.NewServeMux()
	router.SetupRoutes(mux, s, db, rds)

	// start up HTTP
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%d", s.conf.Port),
		Handler: mux,
		BaseContext: func(_ net.Listener) context.Context {
			return context.WithValue(ctx, constant.TokenKeyCtxKey, s.GetConf().TokenKey)
		},
	}

	// Listen from s different goroutine
	go func() {
		if err := srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			s.log.ErrorContext(ctx, "server error", "error", err)
		}

		cancel()
	}()

	s.log.InfoContext(
		ctx,
		"starting api service",
		"port", s.conf.Port,
		"playground", fmt.Sprintf("http://localhost:%d/", s.conf.Port),
	)

	shutdown := make(chan os.Signal, 1)   // Create channel to signify s signal being sent
	signal.Notify(shutdown, os.Interrupt) // When an interrupt is sent, notify the channel

	go func() {
		_ = <-shutdown

		s.log.WarnContext(ctx, "gracefully shutting down...")
		if err := srv.Shutdown(ctx); err != nil {
			s.log.ErrorContext(ctx, "server shutdown error", "error", err)
		}
	}()
}
