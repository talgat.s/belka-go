package router

import (
	"net/http"

	"gitlab.com/talgat.s/belka-go/cmd/api/handler"
	"gitlab.com/talgat.s/belka-go/cmd/api/middleware"
	apiT "gitlab.com/talgat.s/belka-go/cmd/api/types"
	dbT "gitlab.com/talgat.s/belka-go/cmd/db/types"
	rdsT "gitlab.com/talgat.s/belka-go/cmd/rds/types"
	"gitlab.com/talgat.s/belka-go/internal/constant"
)

// SetupRoutes setup router api
func SetupRoutes(mux *http.ServeMux, api apiT.Api, db dbT.DB, rds rdsT.Rds) {
	m := middleware.New(api, db, rds)
	h := handler.New(api, db, rds)

	mux.Handle("/graphql", m.User(h.GraphQL()))
	if api.GetConf().Env != constant.EnvironmentProd {
		mux.Handle("/", h.Playground("/graphql"))
	}
}
