package middleware

import (
	"context"
	"net/http"
	"strings"

	"github.com/99designs/gqlgen/graphql"
	"github.com/vektah/gqlparser/v2/gqlerror"

	"gitlab.com/talgat.s/belka-go/internal/auth"
	"gitlab.com/talgat.s/belka-go/internal/constant"
	"gitlab.com/talgat.s/belka-go/internal/httputils"
)

func (m *middlewareObject) User(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authHeader := r.Header.Get(string(constant.Authorization))

		// Allow unauthenticated users in
		if authHeader == "" {
			next.ServeHTTP(w, r)
			return
		}

		// get token key
		tokenKey, ok := r.Context().Value(constant.TokenKeyCtxKey).(string)
		if !ok {
			errList := gqlerror.List{
				&gqlerror.Error{
					Path:    graphql.GetPath(r.Context()),
					Message: "token key was not provided",
					Extensions: map[string]interface{}{
						"status":     http.StatusUnauthorized,
						"statusText": http.StatusText(http.StatusUnauthorized),
					},
				},
			}
			response := graphql.Response{
				Errors: errList,
			}
			httputils.ErrorJSON(w, response, http.StatusUnauthorized)
			return
		}

		authParts := strings.Split(authHeader, " ")

		if len(authParts) != 2 || strings.ToLower(authParts[0]) != "bearer" {
			errList := gqlerror.List{
				&gqlerror.Error{
					Path:    graphql.GetPath(r.Context()),
					Message: "invalid token",
					Extensions: map[string]interface{}{
						"status":     http.StatusUnauthorized,
						"statusText": http.StatusText(http.StatusUnauthorized),
					},
				},
			}
			response := graphql.Response{
				Errors: errList,
			}
			httputils.ErrorJSON(w, response, http.StatusUnauthorized)
			return
		}

		user, err := auth.ParseAccessToken(tokenKey, authParts[1])

		if err != nil {
			errList := gqlerror.List{
				&gqlerror.Error{
					Path:    graphql.GetPath(r.Context()),
					Message: "invalid token",
					Extensions: map[string]interface{}{
						"status":     http.StatusForbidden,
						"statusText": http.StatusText(http.StatusForbidden),
					},
				},
			}
			response := graphql.Response{
				Errors: errList,
			}
			httputils.ErrorJSON(w, response, http.StatusForbidden)
			return
		}

		// put it in context
		ctx := context.WithValue(r.Context(), constant.UserCtxKey, user)

		// and call the next with our new context
		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})

}
