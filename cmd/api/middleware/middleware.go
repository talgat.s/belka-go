package middleware

import (
	"net/http"

	"gitlab.com/talgat.s/belka-go/cmd/api/types"
	dbT "gitlab.com/talgat.s/belka-go/cmd/db/types"
	rdsT "gitlab.com/talgat.s/belka-go/cmd/rds/types"
)

type Middleware interface {
	User(next http.Handler) http.Handler
}

type middlewareObject struct {
	api types.Api
	db  dbT.DB
	rds rdsT.Rds
}

func New(api types.Api, db dbT.DB, rds rdsT.Rds) Middleware {
	return &middlewareObject{
		api: api,
		db:  db,
		rds: rds,
	}
}
