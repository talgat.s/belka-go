package types

import (
	"context"
	"log/slog"

	dbT "gitlab.com/talgat.s/belka-go/cmd/db/types"
	rdsT "gitlab.com/talgat.s/belka-go/cmd/rds/types"
	"gitlab.com/talgat.s/belka-go/configs"
)

type Api interface {
	Start(ctx context.Context, cancel context.CancelFunc, d dbT.DB, r rdsT.Rds)
	GetConf() *configs.ApiConfig
	GetLog() *slog.Logger
}
