package db

import (
	"log/slog"

	"gitlab.com/talgat.s/belka-go/cmd/db/model"
	"gitlab.com/talgat.s/belka-go/cmd/db/types"
	"gitlab.com/talgat.s/belka-go/configs"
)

func New(log *slog.Logger, conf *configs.DBConfig) (types.DB, error) {
	return model.New(log, conf)
}
