package types

import (
	"context"
)

type DB interface {
	Login(ctx context.Context, email, password string) (User, error)
	Register(ctx context.Context, email, password string) (User, error)
	SaveToken(ctx context.Context, userID, token string) error
	ValidateToken(ctx context.Context, userID, token string) error
	GetUserByID(ctx context.Context, id string) (User, error)
}

type User struct {
	ID        string
	Email     string
	Role      string
	CreatedAt string
	UpdatedAt string
}
