package seeds

import (
	"context"
	"database/sql"

	"gitlab.com/talgat.s/belka-go/cmd/db/database"
	"gitlab.com/talgat.s/belka-go/configs"
)

type Seed interface {
	Populate() error
}

type seeder struct {
	db *sql.DB
}

func New() (Seed, error) {
	conf, err := configs.NewConfig(context.Background())
	if err != nil {
		return nil, err
	}

	db, err := database.NewDB(
		conf.DB.Host,
		conf.DB.Port,
		conf.DB.User,
		conf.DB.Password,
		conf.DB.DBName,
	)
	if err != nil {
		return nil, err
	}

	return &seeder{
		db,
	}, nil
}

func (s *seeder) Populate() error {
	if err := s.users(); err != nil {
		return err
	}

	return s.db.Close()
}
