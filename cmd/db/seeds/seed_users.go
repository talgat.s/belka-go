package seeds

import (
	"log"
)

type user struct {
	id        string
	email     string
	password  string
	role      string
	createdAt string
	updatedAt string
}

func (s *seeder) users() error {
	users := []*user{
		{
			email:    "admin@admin.com",
			password: "admin",
			role:     "Admin",
		},
		{
			email:    "user@user.com",
			password: "user",
			role:     "User",
		},
	}

	sqlQuery := `
		INSERT INTO users (email, password, role)
		VALUES ($1, crypt($2, gen_salt('bf')), UPPER($3));
	`

	tx, err := s.db.Begin()
	if err != nil {
		return err
	}
	sqlStmt, err := tx.Prepare(sqlQuery)
	if err != nil {
		return err
	}

	for _, user := range users {
		_, err := sqlStmt.Exec(user.email, user.password, user.role)
		if err != nil {
			err := tx.Rollback()
			if err != nil {
				log.Fatal(err)
			}
			return err
		}
	}

	err = tx.Commit()
	if err != nil {
		err := tx.Rollback()
		if err != nil {
			log.Fatal(err)
		}
		return err
	}

	return nil
}
