package model

import (
	"database/sql"
	"log/slog"

	"gitlab.com/talgat.s/belka-go/cmd/db/database"
	"gitlab.com/talgat.s/belka-go/cmd/db/types"
	"gitlab.com/talgat.s/belka-go/configs"
)

type model struct {
	log  *slog.Logger
	conf *configs.DBConfig
	db   *sql.DB
}

func New(log *slog.Logger, conf *configs.DBConfig) (types.DB, error) {
	db, err := database.NewDB(
		conf.Host,
		conf.Port,
		conf.User,
		conf.Password,
		conf.DBName,
	)

	if err != nil {
		return nil, err
	}

	m := &model{
		log:  log,
		conf: conf,
		db:   db,
	}

	return m, nil
}
