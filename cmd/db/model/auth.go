package model

import (
	"context"
	"database/sql"
	"fmt"

	"gitlab.com/talgat.s/belka-go/cmd/db/types"
)

func (m *model) Login(ctx context.Context, email, password string) (types.User, error) {
	m.log.InfoContext(ctx, "start Login", "email", email)

	sqlStatement := `
		SELECT
			id,
			email,
			role,
			created_at,
			updated_at
		FROM users
		WHERE
			email=$1
		AND
			password=crypt($2, password);
	`

	row := m.db.QueryRowContext(ctx, sqlStatement, email, password)

	var user types.User

	if err := row.Scan(&user.ID, &user.Email, &user.Role, &user.CreatedAt, &user.UpdatedAt); err != nil {
		m.log.ErrorContext(
			ctx,
			"fail Login",
			"error", err,
			"email", email,
		)
		return user, err
	}

	if err := row.Err(); err != nil {
		m.log.ErrorContext(
			ctx,
			"fail Login",
			"error", err,
			"email", email,
		)
		return user, err
	}

	m.log.InfoContext(ctx, "success Login", "user", user)
	return user, nil
}

func (m *model) Register(ctx context.Context, email, password string) (types.User, error) {
	m.log.InfoContext(ctx, "start Register", "email", email)

	sqlStatement := `
		INSERT INTO users (email, password)
		VALUES ($1, crypt($2, gen_salt('bf')))
		RETURNING id, email, role, created_at, updated_at;
	`

	row := m.db.QueryRowContext(ctx, sqlStatement, email, password)

	var user types.User

	if err := row.Scan(&user.ID, &user.Email, &user.Role, &user.CreatedAt, &user.UpdatedAt); err != nil {
		m.log.ErrorContext(ctx, "fail Register", "error", err, "email", email)
		return user, err
	}

	if err := row.Err(); err != nil {
		m.log.ErrorContext(
			ctx,
			"error Register",
			"error", err,
			"email", email,
		)
		return user, err
	}

	m.log.InfoContext(ctx, "success Register", "user", user)
	return user, nil
}

func (m *model) SaveToken(ctx context.Context, userID, token string) error {
	m.log.InfoContext(ctx, "start SaveToken", "userID", userID)

	sqlStatement := `
		INSERT INTO tokens (user_id, token)
		VALUES ($1, crypt($2, gen_salt('bf')))
		ON CONFLICT (user_id)
		DO
		    UPDATE SET token = EXCLUDED.token || ';' || tokens.token;
	`

	res, err := m.db.ExecContext(ctx, sqlStatement, userID, token)
	if err != nil {
		m.log.ErrorContext(ctx, "fail SaveToken", "error", err, "userID", userID)
		return err
	}

	count, err := res.RowsAffected()
	if err != nil {
		m.log.ErrorContext(ctx, "fail SaveToken", "error", err, "userID", userID)
		return err
	}

	if count == 0 {
		m.log.ErrorContext(ctx, "fail SaveToken", "error", fmt.Errorf("no records were updated"))
		return sql.ErrNoRows
	}

	m.log.InfoContext(ctx, "success SaveToken", "userID", userID)
	return nil
}

func (m *model) ValidateToken(ctx context.Context, userID, token string) error {
	m.log.InfoContext(ctx, "start ValidateToken", "userID", userID)

	sqlStatement := `
		SELECT token
		FROM tokens
		WHERE
		    user_id=$1
		AND
		    token=crypt($2, token);
	`

	res, err := m.db.ExecContext(ctx, sqlStatement, userID, token)
	if err != nil {
		m.log.ErrorContext(ctx, "fail ValidateToken", "error", err, "userID", userID)
		return err
	}

	count, err := res.RowsAffected()
	if err != nil {
		m.log.ErrorContext(ctx, "fail ValidateToken", "error", err, "userID", userID)
		return err
	}

	if count == 0 {
		m.log.ErrorContext(ctx, "fail ValidateToken", "error", fmt.Errorf("no records were updated"))
		return sql.ErrNoRows
	}

	m.log.InfoContext(ctx, "success ValidateToken", "userID", userID)
	return nil
}

func (m *model) GetUserByID(ctx context.Context, id string) (types.User, error) {
	m.log.InfoContext(ctx, "start GetUserByID", "id", id)

	sqlStatement := `
		SELECT
			id,
			email,
			role,
			created_at,
			updated_at
		FROM users
		WHERE
			id=$1;
	`

	row := m.db.QueryRowContext(ctx, sqlStatement, id)

	var user types.User

	if err := row.Scan(&user.ID, &user.Email, &user.Role, &user.CreatedAt, &user.UpdatedAt); err != nil {
		m.log.ErrorContext(
			ctx,
			"fail GetUserByID",
			"error", err,
			"id", id,
		)
		return user, err
	}

	if err := row.Err(); err != nil {
		m.log.ErrorContext(
			ctx,
			"fail GetUserByID",
			"error", err,
			"id", id,
		)
		return user, err
	}

	m.log.InfoContext(ctx, "success GetUserByID", "user", user)
	return user, nil
}
