FROM golang:1.21-alpine AS builder
WORKDIR /app
COPY . .
RUN go build -o api .

FROM alpine AS runner

# Add Maintainer Info
LABEL maintainer="Talgat Saribayev <talgat.s@protonmail.com>"

WORKDIR /app
COPY --from=builder /app/api api
EXPOSE 80
CMD ["/app/api"]

