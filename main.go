package main

import (
	"context"

	"gitlab.com/talgat.s/belka-go/cmd/api"
	"gitlab.com/talgat.s/belka-go/cmd/db"
	"gitlab.com/talgat.s/belka-go/cmd/rds"
	"gitlab.com/talgat.s/belka-go/configs"
	"gitlab.com/talgat.s/belka-go/internal/constant"
	"gitlab.com/talgat.s/belka-go/pkg/logger"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())

	// conf
	conf, err := configs.NewConfig(ctx)
	if err != nil {
		panic(err)
	}

	// setup logger
	log := logger.New(conf)

	// configure redis service
	r := rds.New(log.With("service", constant.Rds), conf.Rds)
	log.InfoContext(ctx, "initialize service", "service", "redis")

	// configure db service
	d, err := db.New(log.With("service", constant.DB), conf.DB)
	if err != nil {
		log.ErrorContext(
			ctx,
			"initialize service error",
			"service", "database",
			"error", err,
		)
		panic(err)
	}
	log.InfoContext(ctx, "initialize service", "service", "database")

	// configure gateway service
	srv := api.New(log.With("service", constant.Api), conf.Api)
	log.InfoContext(ctx, "initialize service", "service", "api")
	// start gateway service
	srv.Start(ctx, cancel, d, r)

	<-ctx.Done()
	// Your cleanup tasks go here
	log.InfoContext(ctx, "cleaning up ...")

	if err := r.GetPool().Close(); err != nil {
		log.ErrorContext(ctx, "fail redis close", "error", err)
	}
	log.InfoContext(ctx, "successfully closed redis")

	log.InfoContext(ctx, "server was successful shutdown.")
}
