package scalar

import (
	"encoding/json"
	"fmt"
	"io"
	"strconv"
)

type Score int

// UnmarshalGQL implements the graphql.Unmarshaler interface
func (s *Score) UnmarshalGQL(v interface{}) error {
	switch val := v.(type) {
	case int:
		*s = Score(val)
		return nil
	case float64:
		*s = Score(val)
		return nil
	case string:
		if i, err := strconv.Atoi(val); err != nil {
			return err
		} else {
			*s = Score(i)
			return nil
		}
	case json.Number:
		if i64, err := val.Int64(); err != nil {
			return err
		} else {
			*s = Score(i64)
			return nil
		}
	default:
		return fmt.Errorf("scalar Suit must be an integer")
	}
}

// MarshalGQL implements the graphql.Marshaler interface
func (s Score) MarshalGQL(w io.Writer) {
	_, _ = fmt.Fprintf(w, "%d", s)
}
