package scalar

import (
	"encoding/json"
	"fmt"
	"io"
	"strconv"
)

type Rank int

// UnmarshalGQL implements the graphql.Unmarshaler interface
func (r *Rank) UnmarshalGQL(v interface{}) error {
	switch val := v.(type) {
	case int:
		*r = Rank(val)
		return nil
	case float64:
		*r = Rank(val)
		return nil
	case string:
		if i, err := strconv.Atoi(val); err != nil {
			return err
		} else {
			*r = Rank(i)
			return nil
		}
	case json.Number:
		if i64, err := val.Int64(); err != nil {
			return err
		} else {
			*r = Rank(i64)
			return nil
		}
	default:
		return fmt.Errorf("scalar Suit must be an integer")
	}
}

// MarshalGQL implements the graphql.Marshaler interface
func (r Rank) MarshalGQL(w io.Writer) {
	_, _ = fmt.Fprintf(w, "%d", r)
}
