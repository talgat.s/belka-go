package scalar

import (
	"encoding/json"
	"fmt"
	"io"
	"strconv"
)

type Suit int

// UnmarshalGQL implements the graphql.Unmarshaler interface
func (s *Suit) UnmarshalGQL(v interface{}) error {
	switch val := v.(type) {
	case int:
		*s = Suit(val)
		return nil
	case float64:
		*s = Suit(val)
		return nil
	case string:
		if i, err := strconv.Atoi(val); err != nil {
			return err
		} else {
			*s = Suit(i)
			return nil
		}
	case json.Number:
		if i64, err := val.Int64(); err != nil {
			return err
		} else {
			*s = Suit(i64)
			return nil
		}
	default:
		return fmt.Errorf("scalar Suit must be an integer")
	}
}

// MarshalGQL implements the graphql.Marshaler interface
func (s Suit) MarshalGQL(w io.Writer) {
	_, _ = fmt.Fprintf(w, "%d", s)
}
