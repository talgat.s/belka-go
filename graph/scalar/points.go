package scalar

import (
	"encoding/json"
	"fmt"
	"io"
	"strconv"
)

type Points int

// UnmarshalGQL implements the graphql.Unmarshaler interface
func (p *Points) UnmarshalGQL(v interface{}) error {
	switch val := v.(type) {
	case int:
		*p = Points(val)
		return nil
	case float64:
		*p = Points(val)
		return nil
	case string:
		if i, err := strconv.Atoi(val); err != nil {
			return err
		} else {
			*p = Points(i)
			return nil
		}
	case json.Number:
		if i64, err := val.Int64(); err != nil {
			return err
		} else {
			*p = Points(i64)
			return nil
		}
	default:
		return fmt.Errorf("scalar Suit must be an integer")
	}
}

// MarshalGQL implements the graphql.Marshaler interface
func (p Points) MarshalGQL(w io.Writer) {
	_, _ = fmt.Fprintf(w, "%d", p)
}
