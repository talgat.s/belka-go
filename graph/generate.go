//go:build ignore
// +build ignore

// NOTE: need for codegen. Check Makefile.
// NOTE: disabled for now need to change go gnerate command in graph/main.go

package main

import (
	"fmt"
	"os"

	"github.com/99designs/gqlgen/api"
	"github.com/99designs/gqlgen/codegen/config"
	"github.com/99designs/gqlgen/plugin/modelgen"
)

// NOTE: Defining mutation function
func mutateTagOmitemptyHook(b *modelgen.ModelBuild) *modelgen.ModelBuild {
	for _, model := range b.Models {
		for _, field := range model.Fields {
			field.Tag = field.Tag[:len(field.Tag)-1] + `,omitempty"`
		}
	}

	return b
}

func main() {
	cfg, err := config.LoadConfigFromDefaultLocations()
	if err != nil {
		fmt.Fprintln(os.Stderr, "failed to load config", err.Error())
		os.Exit(2)
	}

	// NOTE: Attaching the mutation function onto modelgen plugin
	p := modelgen.Plugin{
		MutateHook: mutateTagOmitemptyHook,
	}

	err = api.Generate(
		cfg,
		api.ReplacePlugin(&p),
	)
	if err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(3)
	}
}
