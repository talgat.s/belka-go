package resolver

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.
// Code generated by github.com/99designs/gqlgen version v0.17.40

import (
	"context"
	"fmt"

	_map "gitlab.com/talgat.s/belka-go/graph/map"
	"gitlab.com/talgat.s/belka-go/graph/model"
)

// Hands is the resolver for the hands field.
func (r *queryResolver) Hands(ctx context.Context, gameID string) ([]*model.Hand, error) {
	r.Log.InfoContext(ctx, "start query Hands", "gameID", gameID)

	if r.Games[gameID] == nil {
		r.Log.ErrorContext(
			ctx,
			"fail query Hands",
			"error", fmt.Errorf("game %s does not exists", gameID),
		)
		return nil, fmt.Errorf("no game with id %s, please contact developers", gameID)
	}

	if len(r.Games[gameID].GetRounds()) == 0 {
		r.Log.ErrorContext(
			ctx,
			"fail query Hands",
			"error", fmt.Errorf("game %s does not have subscriptions", gameID),
		)
		return nil, fmt.Errorf("no rounds in game with id %s, please contact developers", gameID)
	}

	for p, h := range r.Games[gameID].GetRounds()[0].GetHands() {
		fmt.Printf("player %+v\n", p)
		fmt.Printf("hand %+v\n", h)
		fmt.Println("-------------------")
	}

	result := _map.GamePlayersHandToPlayerHands(r.Games[gameID].GetRounds()[0].GetHands())

	r.Log.InfoContext(ctx, "success query Hands")

	return result, nil
}
