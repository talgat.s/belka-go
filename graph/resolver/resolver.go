package resolver

import (
	"log/slog"

	dbT "gitlab.com/talgat.s/belka-go/cmd/db/types"
	rdsT "gitlab.com/talgat.s/belka-go/cmd/rds/types"
	"gitlab.com/talgat.s/belka-go/configs"
	"gitlab.com/talgat.s/belka-go/internal/game"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	Log  *slog.Logger
	Conf *configs.ApiConfig
	DB   dbT.DB
	Rds  rdsT.Rds
	// TODO: remove
	Games map[string]game.Game
}
