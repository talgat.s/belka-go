package directive

import (
	"context"
	"fmt"
	"slices"

	"github.com/99designs/gqlgen/graphql"

	"gitlab.com/talgat.s/belka-go/graph/model"
	"gitlab.com/talgat.s/belka-go/internal/auth"
	"gitlab.com/talgat.s/belka-go/internal/constant"
	"gitlab.com/talgat.s/belka-go/internal/enum"
)

func AllowAccess(
	ctx context.Context,
	_ interface{},
	next graphql.Resolver,
	roles []model.Role,
) (interface{}, error) {
	user, ok := ctx.Value(constant.UserCtxKey).(*auth.User)

	if !ok || user == nil || !slices.Contains(enum.SliceToStrSlice(roles), user.Role) {
		// block calling the next resolver
		return nil, fmt.Errorf("access denied")
	}

	// pass through
	return next(ctx)
}
