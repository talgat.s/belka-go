package _map

import (
	"strings"

	"gitlab.com/talgat.s/belka-go/graph/model"
)

func RoleToGQLRole(role string) model.Role {
	switch strings.ToLower(role) {
	case "admin":
		return model.RoleAdmin
	case "user":
	default:
		return model.RoleUser
	}

	return model.RoleUser
}
