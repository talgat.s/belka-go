package _map

import (
	"github.com/google/uuid"

	"gitlab.com/talgat.s/belka-go/graph/model"
	"gitlab.com/talgat.s/belka-go/graph/scalar"
	"gitlab.com/talgat.s/belka-go/internal/game"
)

func GameGamesToGames(gs []game.Game) []*model.Game {
	games := make([]*model.Game, len(gs))

	for i, g := range gs {
		games[i] = GameGameToGame(g)
	}

	return games
}

func GameGameToGame(g game.Game) *model.Game {
	if g == nil {
		return nil
	}

	return &model.Game{
		ID:     GameIDToID(g.GetID()),
		Teams:  GameTeamsToTeams(g.GetTeams()),
		Winner: GameTeamToTeam(g.GetWinner()),
	}
}

func GameRoundsToRounds(rs []game.Round) []*model.Round {
	rounds := make([]*model.Round, len(rs))

	for i, r := range rs {
		rounds[i] = GameRoundToRound(r)
	}

	return rounds
}

func GameRoundToRound(r game.Round) *model.Round {
	if r == nil {
		return nil
	}

	return &model.Round{
		ID:          GameIDToID(r.GetID()),
		TeamsPoints: GamePointsToTeamsPoints(r.GetPoints()),
		Winner:      GameTeamToTeam(r.GetWinner()),
	}
}

func GameLapsToLaps(ls []game.Lap) []*model.Lap {
	laps := make([]*model.Lap, len(ls))

	for i, l := range ls {
		laps[i] = GameLapToLap(l)
	}

	return laps
}

func GameLapToLap(l game.Lap) *model.Lap {
	if l == nil {
		return nil
	}

	return &model.Lap{
		ID: GameIDToID(l.GetID()),
	}
}

func GameMovesToMoves(ms []game.Move) []*model.Move {
	moves := make([]*model.Move, len(ms))

	for i, l := range ms {
		moves[i] = GameMoveToMove(l)
	}

	return moves
}

func GameMoveToMove(m game.Move) *model.Move {
	if m == nil {
		return nil
	}

	return &model.Move{
		ID:     GameIDToID(m.GetID()),
		Player: GamePlayerToPlayer(m.GetPlayer()),
		Card:   GameCardToCard(m.GetCard()),
	}
}

func GameTeamsToTeams(ts [game.NumTeamsPerGame]game.Team) []*model.Team {
	teams := make([]*model.Team, len(ts))

	for i, t := range ts {
		teams[i] = GameTeamToTeam(t)
	}

	return teams
}

func GameTeamToTeam(t game.Team) *model.Team {
	if t == nil {
		return nil
	}

	return &model.Team{
		ID:      GameIDToID(t.GetID()),
		Players: GameMembersToPlayer(t.GetMembers()),
	}
}

func GameMembersToPlayer(ps [game.NumPlayersPerTeam]game.Player) []model.Player {
	players := make([]model.Player, len(ps))

	for i, p := range ps {
		players[i] = GamePlayerToPlayer(p)
	}

	return players
}

func GamePlayerToPlayer(p game.Player) model.Player {
	if p == nil {
		return nil
	}

	switch v := p.(type) {
	case *game.User:
		return &model.UserPlayer{
			ID:   GameIDToID(v.GetID()),
			Name: v.GetName(),
		}
	case *game.Bot:
		return &model.BotPlayer{
			ID:   GameIDToID(v.GetID()),
			Name: v.GetName(),
		}
	}

	return nil
}

func GameHandToCards(h *game.Hand) []*model.Card {
	cards := make([]*model.Card, len(h))

	for i, c := range h {
		if c != nil {
			cards[i] = GameCardToCard(*c)
		} else {
			cards[i] = nil
		}
	}

	return cards
}

func GameCardToCard(c game.Card) *model.Card {
	return &model.Card{
		Suit: GameSuitToSuit(c.GetSuit()),
		Rank: GameRankToRank(c.GetRank()),
	}
}

func GamePointsToTeamsPoints(tp map[game.Team]game.Points) []*model.TeamPoints {
	var teamsPoints []*model.TeamPoints

	for t, p := range tp {
		teamPoints := &model.TeamPoints{
			Team:   GameTeamToTeam(t),
			Points: scalar.Points(p),
		}
		teamsPoints = append(teamsPoints, teamPoints)
	}

	return teamsPoints
}

func GamePlayersSuitToPlayersSuit(ps map[game.Player]game.Suit) []*model.PlayerSuit {
	playerSuits := make([]*model.PlayerSuit, len(ps))

	for p, s := range ps {
		playerSuit := &model.PlayerSuit{
			Player: GamePlayerToPlayer(p),
			Suit:   GameSuitToSuit(s),
		}
		playerSuits = append(playerSuits, playerSuit)
	}

	return playerSuits
}

func GameTeamsScoreToTeamsScore(ts game.TeamsScore) []*model.TeamScore {
	teamScores := make([]*model.TeamScore, game.NumTeamsPerGame)

	for _, t := range ts.GetTeams() {
		playerSuit := &model.TeamScore{
			Team:  GameTeamToTeam(t),
			Score: scalar.Score(ts.GetScore(t)),
		}
		teamScores = append(teamScores, playerSuit)
	}

	return teamScores
}

func GamePlayersHandToPlayerHands(ps map[game.Player]*game.Hand) []*model.Hand {
	var hands []*model.Hand

	for p, h := range ps {
		hand := &model.Hand{
			Player: GamePlayerToPlayer(p),
			Cards:  GameHandToCards(h),
		}
		hands = append(hands, hand)
	}

	return hands
}

func GameSuitToSuit(s game.Suit) scalar.Suit {
	return scalar.Suit(s)
}

func GameRankToRank(r game.Rank) scalar.Rank {
	return scalar.Rank(r)
}

func GameIDToID(id uuid.UUID) string {
	return id.String()
}

func InputCardToGameCard(c *model.InputCard) game.Card {
	return game.NewCard(SuitToGameSuit(c.Suit), RankToGameRank(c.Rank))
}

func SuitToGameSuit(s *scalar.Suit) game.Suit {
	return game.Suit(*s)
}

func RankToGameRank(r *scalar.Rank) game.Rank {
	return game.Rank(*r)
}
