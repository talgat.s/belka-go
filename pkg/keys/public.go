package keys

import (
	"crypto"
	"crypto/x509"
	"encoding/pem"
)

func StringifyPublic(public crypto.PublicKey) ([]byte, error) {
	block, err := getPublicBlock(public)
	if err != nil {
		return nil, err
	}
	pemEncode := pem.EncodeToMemory(block)

	return pemEncode, nil
}

func getPublicBlock(public crypto.PublicKey) (*pem.Block, error) {
	bytes, err := x509.MarshalPKIXPublicKey(public)
	if err != nil {
		return nil, err
	}

	return &pem.Block{
		Type:  "RSA PUBLIC KEY",
		Bytes: bytes,
	}, nil
}
