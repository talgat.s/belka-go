package keys

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
)

func StringifyPrivate(private *rsa.PrivateKey) []byte {
	return pem.EncodeToMemory(getPrivateBlock(private))
}

func getPrivateBlock(private *rsa.PrivateKey) *pem.Block {
	bytes := x509.MarshalPKCS1PrivateKey(private)

	return &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: bytes,
	}
}
